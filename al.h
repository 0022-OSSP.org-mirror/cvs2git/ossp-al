/*
**  OSSP al -- Assembly Line
**  Copyright (c) 2002-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002-2005 Cable & Wireless <http://www.cw.com/>
**  Copyright (c) 2002-2005 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002-2005 Michael van Elst <mlelstv@serpens.de>
**
**  This file is part of OSSP al, an abstract datatype of a data buffer
**  that can assemble, move and truncate data but avoids actual copying
**  and which can be found at http://www.ossp.org/pkg/lib/al/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  al.h: assembly line public API definition
*/

#ifndef __AL_H__
#define __AL_H__

typedef enum {
    AL_OK,
    AL_ERR_ARG,
    AL_ERR_MEM,
    AL_ERR_EOF,
    AL_ERR_INT
} al_rc_t;

struct al_st;
typedef struct al_st al_t;

struct al_chunk_st;
typedef struct al_chunk_st al_chunk_t;

typedef void *al_label_t;

typedef enum {
    AL_FORWARD,
    AL_BACKWARD,
    AL_FORWARD_SPAN,
    AL_BACKWARD_SPAN
} al_td_t;

struct al_tx_st;
typedef struct al_tx_st al_tx_t;

al_rc_t al_create       (al_t **alp);
al_rc_t al_destroy      (al_t *al);
al_rc_t al_append_bytes (al_t *al, const char *src, size_t n, al_label_t label);
al_rc_t al_prepend_bytes(al_t *al, const char *src, size_t n, al_label_t label);
al_rc_t al_attach_buffer(al_t *al, char *p, size_t n, al_label_t label, void (*freemem)(char *, size_t, void *), void *u);
al_rc_t al_txalloc      (al_t *al, al_tx_t **txp);
al_rc_t al_txfree       (al_t *al, al_tx_t *tx);
al_rc_t al_traverse     (al_t *al, size_t off, size_t n, al_td_t dir, al_label_t label, al_tx_t *tx);
al_rc_t al_traverse_next(al_t *al, al_tx_t *tx, al_chunk_t **alcp);
al_rc_t al_traverse_end (al_t *al, al_tx_t *tx, int final);
al_rc_t al_traverse_cb  (al_t *al, size_t off, size_t n, al_td_t dir, al_label_t label, al_rc_t (*cb)(al_chunk_t *, void *), void *u);
al_rc_t al_copy         (al_t *al, size_t off, size_t n, al_label_t label, al_t *tal);
al_rc_t al_splice       (al_t *al, size_t off, size_t n, al_t *nal, al_t *tal);
al_rc_t al_setlabel     (al_t *al, size_t off, size_t n, al_label_t oldlabel, al_label_t newlabel);
al_rc_t al_flatten      (al_t *al, size_t off, size_t n, al_td_t dir, al_label_t label, char *dst, size_t *lenp);
al_rc_t al_firstlabel   (al_t *al, size_t off, size_t n, al_td_t dir, al_label_t label, al_label_t *labelp);
al_rc_t al_spanlabel    (al_t *al, size_t off, size_t n, al_label_t label, size_t *offp, size_t *spanp);

size_t     al_bytes      (const al_t *al);
size_t     al_chunk_len  (al_chunk_t *alc);
al_label_t al_chunk_label(al_chunk_t *alc);
int        al_same_label (al_chunk_t *alc, al_label_t label);
size_t     al_chunk_span (al_chunk_t *alc, size_t off, size_t n);
char      *al_chunk_ptr  (al_chunk_t *alc, size_t off);

const char *al_error      (al_rc_t rc);

#endif /* __AL_H__ */

